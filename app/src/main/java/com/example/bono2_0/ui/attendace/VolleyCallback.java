package com.example.bono2_0.ui.attendace;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface VolleyCallback {

    void onSuccessResponse(JSONObject result);

    void onErrorResponse(VolleyError e);
}
