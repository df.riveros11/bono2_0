package com.example.bono2_0.ui.main;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.bono2_0.R;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class BluetoothTabOne extends Fragment {

    /**
     * Variables FINAL
     */
    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("d-M-yyyy");
    /**
     private static final String URL_TEST = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/5-7-2019/students/201212651";
     */
    private static final String URL = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/";
    private static final String STUDENTS_PATH = "/students";
    private static final String ID_STUDENTS = "/201327471";

    private static final String TAG = "BLUETOOTH_TAB";
    private static final String TAG_DATE = "FormatDate";

    private static final int ENABLE_BT = 0;
    private static final int DISCOVERY_BT = 1;

    /**
     * Botones, texto, que van ha estar en la interfaz del app movil
     */
    private TextView mStatusBlueMAC, mAssistanceMov;
    private ImageView mBlueTv;
    private Button mOnBtn, mOffBtn, mDiscoverBtn, mTryAgain;

    private BluetoothAdapter mBlueAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bluetooth_tab_one, container, false);
        initializeBTI(view);
         /*
          Obtiene la MAC del dispositivo
         */
        this.getMAC();
        /*
          Obtiene si el bluetooth esta disponible
         */
        this.getBluetooth();
        /*
          Pinta la imagen dependiendo de si el bluetooth esta prendido
         */
        this.paintBluetooth();
        /*
            Obtiene si estas inscrito o no en firestore
         */
        this.getAttendance();
        /*
          Prende el bluetooth
         */
        this.getBluetoothOn();
        /*
          Apaga el bluetooth
         */
        this.getBluetoothOff();
        /*
          Hace que el dispositivo sea visible
         */
        this.getDiscoveryOn();
        /*
            Busca denuevo en la base de datos
         */
        mTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAttendance();
            }
        });
        return view;
    }

    private void initializeBTI(View view) {
        mStatusBlueMAC = view.findViewById(R.id.statusBluetoothMAC);
        mAssistanceMov = view.findViewById(R.id.statusAssistanceMov);
        mBlueTv = view.findViewById(R.id.bluetoothTv);
        mOnBtn = view.findViewById(R.id.onBtn);
        mOffBtn = view.findViewById(R.id.offBtn);
        mDiscoverBtn = view.findViewById(R.id.discoverableBtn);
        mTryAgain = view.findViewById(R.id.tryagain);
    }

    private void getDiscoveryOn() {
        mDiscoverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBlueAdapter.isDiscovering()){
                    showMessage("Making Your Device Discoverable");
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    startActivityForResult(intent, DISCOVERY_BT);
                    mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
                }else{
                    showMessage("Bluetooth is already Discoverable");
                }
            }
        });
    }

    private void getBluetoothOff() {
        mOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBlueAdapter.isEnabled()){
                    mBlueAdapter.disable();
                    showMessage("Turning Bluetooth Off");
                    mBlueTv.setImageResource(R.drawable.ic_action_off);
                }else{
                    showMessage("Bluetooth is already off");
                }
            }
        });
    }

    private void getBluetoothOn() {
        mOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBlueAdapter.isEnabled()){
                    showMessage("Turning on Bluetooth");
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, ENABLE_BT);
                }else{
                    showMessage("Bluetooth is already on");
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void getAttendance() {
    /*
      Crea el formato de la fecha del dia
     */
        String dateFormat = getDate();

        /*
          Genera la URL de la base de datos
         */
        String urlCompleted = URL + dateFormat + STUDENTS_PATH + ID_STUDENTS;
        Log.w(TAG, urlCompleted);

        /*
          Busca el ID de estudiante con la fecha establecida
         */
        try {
            RequestQueue que = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, urlCompleted, null, new Response.Listener<JSONObject>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {
                    Log.w(TAG, "FOUND");
                    Log.w(TAG, response.toString());
                    mAssistanceMov.setText("You are registered");
                    mTryAgain.setText("You are registered");
                }
            }, new Response.ErrorListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onErrorResponse(VolleyError e) {
                    Log.w(TAG, "NO ESTA INSCRITO");
                    Log.w(TAG, e.toString());
                    mAssistanceMov.setError(e.toString());
                    mAssistanceMov.setText("Not registered");
                    mTryAgain.setText("Tell to Mario");
                }
            });
            que.add(getRequest);
        } catch(Exception e){
            mAssistanceMov.setError(e.toString());
            mAssistanceMov.setText("ERROR IN EXCEPTION");
            Log.w(TAG, e.toString());
        }
    }

    private String getDate() {
        String dateFormat = "";
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            dateFormat = SIMPLE_DATE_FORMAT.format(calendar.getTime());
            Log.w(TAG_DATE, dateFormat);
        }catch(Exception e){
            Log.w(TAG_DATE, e.toString());
        }
        return dateFormat;
    }

    private void paintBluetooth() {
        if(mBlueAdapter.isEnabled()){
            mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
        }else{
            mBlueTv.setImageResource(R.drawable.ic_action_off);
        }
    }

    @SuppressLint("SetTextI18n")
    private void getBluetooth() {
        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBlueAdapter == null){
            mBlueTv.setImageResource(R.drawable.ic_action_warning);
        }
    }

    @SuppressLint("SetTextI18n")
    private void getMAC() {
        String macAddress = android.provider.Settings.Secure.getString(Objects.requireNonNull(this.getContext()).getContentResolver(), "bluetooth_address");
        if(macAddress != null){
            mStatusBlueMAC.setText(macAddress);
        }else{
            mStatusBlueMAC.setText("It is not possible to find the Bluetooth MAC");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
                showMessage("Bluetooth is on");
            } else {
                showMessage("couldn't on bluetooth");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showMessage(String msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

}
