package com.example.bono2_0.ui.attendace;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.bono2_0.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExpLVAdapter extends BaseExpandableListAdapter {

    private List<String> listCategories;
    private Map<String, List<Student>> mapChild;
    private Context context;
    private static final String TAG_PRUEBA = "PRUEBA_EXPLV";

    public ExpLVAdapter(List<String> listCategories, Map<String, List<Student>> mapChild, Context context) {
        this.listCategories = listCategories;
        this.mapChild = mapChild;
        this.context = context;
        Log.w(TAG_PRUEBA, listCategories.size() + "");
        Log.w(TAG_PRUEBA, mapChild.size() + "");
    }

    @Override
    public int getGroupCount() {
        return listCategories.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return mapChild.get(listCategories.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return listCategories.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return mapChild.get(listCategories.get(i)).get(i1).getId();
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String tituloCategoria = (String) this.getGroup(i);
        view = LayoutInflater.from(context).inflate(R.layout.elv_group, null);
        TextView tvGroup =  (TextView) view.findViewById(R.id.tvGroup);
        tvGroup.setText(tituloCategoria);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        String item = (String) getChild(i,i1);
        view = LayoutInflater.from(context).inflate(R.layout.elv_child, null);
        TextView tvChild = view.findViewById(R.id.tvChild);
        tvChild.setText(item);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
