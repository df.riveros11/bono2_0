package com.example.bono2_0.ui.attendace;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.example.bono2_0.R;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AttendaceTabTwo extends Fragment{

    private ExpandableListView expLV;
    private ExpLVAdapter adapter;
    private List<String> listCategorias;
    private Map<String, List<Student>> mapChild;

    /*
        REAL
     */
    private List<String> fechas;
    private List<Student> listStudents;
    private Map<String, String> students;
    private Map<String, List<Student>> studentsAbscence;
    private static final String URL_STUDENT_LIST = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList?pageSize=100";
    private static final String TAG_SET_STUDENT = "STUDENT";
    private static final String TAG_GET_STUDENT_ABSCANCE = "ABSCENCE";
    private static final String TAG_PRUEBA = "PRUEBA";
    private static final String URL = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/";
    private static final String STUDENTS_PATH = "/students/";

    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("d-M-yyyy");

    private Calendar initCalendar = Calendar.getInstance();
    private Calendar endCalendar = Calendar.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.attendance_tab_two, container, false);
        expLV = (ExpandableListView) view.findViewById(R.id.expLV);
        listCategorias = new ArrayList<>();
        mapChild = new HashMap<>();
        students = new HashMap<>();
        studentsAbscence = new HashMap<>();
        listStudents = new ArrayList<>();
        fechas = new ArrayList<>();
        this.getStudents();
        saveData(view);
        return view;
    }

    private void setDates() {
        initCalendar.set(2019, 7, 5);
        endCalendar.set(2019, 7, 31);
    }

    private void getStudents() {
            this.httpRequest(Request.Method.GET, URL_STUDENT_LIST, null, new VolleyCallback(){

                @Override
                public void onSuccessResponse(JSONObject response) {
                    try {
                        getStudentsList(response);
                        setDates();
                    } catch (JSONException e) {
                        Log.w(TAG_SET_STUDENT, e.toString());
                    }
                    Log.w(TAG_GET_STUDENT_ABSCANCE, "VOY A ENTRAR");
                            getStudentAbscence();
                }

                @Override
                public void onErrorResponse(VolleyError e) {
                    Toast.makeText(getContext(),"Predetermined information is set", Toast.LENGTH_SHORT).show();
                    Log.w(TAG_PRUEBA, "CARGAR DATA PREESTABLECIDA");
                    Log.w(TAG_PRUEBA, e.toString());
                    setDates();
                    setData();
                }
            });
        }

    private void getStudentsList(JSONObject response) throws JSONException {
        JSONArray document = response.getJSONArray("documents");
        for (int i = 0; i < document.length() ;i++){
            JSONObject jsonObject = document.getJSONObject(i);
            JSONObject fields = null;
            try {
                fields = jsonObject.getJSONObject("fields");
            } catch (Exception e){
                Log.w(TAG_SET_STUDENT, e.toString());
            }
            if(fields != null){
                String name = fields.getJSONObject("name").getString("stringValue");
                String id = fields.getJSONObject("code").getString("stringValue");
                listStudents.add(new Student(name,id));
                students.put(id, name);
            }
        }
    }

    private void getStudentAbscence() {
        Log.w(TAG_GET_STUDENT_ABSCANCE, !endCalendar.before(initCalendar) +"" );
        for (Calendar init = initCalendar; !endCalendar.before(initCalendar) ; init.add(Calendar.DATE, 1)){
            if(init.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY || init.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY || init.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                final String fecha = SIMPLE_DATE_FORMAT.format(init.getTime());
                fechas.add(fecha);
                Log.w(TAG_GET_STUDENT_ABSCANCE, fecha);
                Set<String> studentsID = students.keySet();
                for (final String studentID: studentsID) {
                    String url = URL + fecha + STUDENTS_PATH + studentID;
                    Log.w(TAG_GET_STUDENT_ABSCANCE, url);
                    Log.w(TAG_GET_STUDENT_ABSCANCE, fecha + " " + studentID);
                    this.httpRequest(Request.Method.GET, URL_STUDENT_LIST, null, new VolleyCallback() {

                        @Override
                        public void onSuccessResponse(JSONObject response) {}

                        @Override
                        public void onErrorResponse(VolleyError e) {
                            Log.w(TAG_GET_STUDENT_ABSCANCE, e.toString());
                            Log.w(TAG_GET_STUDENT_ABSCANCE, "NO ESTA INSCRITO");
                            Student student = new Student(students.get(studentID),studentID);
                            List<Student> students = new LinkedList<>();
                            students.add(student);
                            studentsAbscence.put(fecha, students);
                        }
                    });
                }
            }
        }
        try {
            //Ponemos a "Dormir" el programa durante los ms que queremos
            Thread.sleep(100*1000);
        } catch (Exception e) {
            System.out.println(e);
        }
        adapter = new ExpLVAdapter(fechas, studentsAbscence, getContext());
        expLV.setAdapter(adapter);
    }


    private void setData() {

        Student studentA = new Student("Oliver Amaya", "201212651");
        Student studentB = new Student("Diego Sanabria ", "201217484 ");
        Student studentC = new Student("Juan Nicolas Galvis", "201313973");
        Student studentD = new Student("Sergio Velasquez", "201315851");

        List<Student> students = new ArrayList<>();
        students.add(studentA);
        students.add(studentB);
        students.add(studentC);
        students.add(studentD);

        listStudents.add(studentA);
        listStudents.add(studentB);
        listStudents.add(studentC);
        listStudents.add(studentD);

        Log.w(TAG_PRUEBA, "LOL");
        Log.w(TAG_PRUEBA, SIMPLE_DATE_FORMAT.format(initCalendar.getTime()));
        Log.w(TAG_PRUEBA, !endCalendar.before(initCalendar) + "");

        for (Calendar init = initCalendar; !endCalendar.before(initCalendar) ; init.add(Calendar.DATE, 1)){
            if(init.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY ||
                    init.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY ||
                    init.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                listCategorias.add(SIMPLE_DATE_FORMAT.format(init.getTime()));
                mapChild.put(SIMPLE_DATE_FORMAT.format(init.getTime()), students);
            }
        }

        Log.w(TAG_PRUEBA, listCategorias.size() + "");
        Log.w(TAG_PRUEBA, mapChild.size() + "");
        adapter = new ExpLVAdapter(listCategorias, mapChild, getContext());
        expLV.setAdapter(adapter);
    }

    private void saveData(View view) {
        ImageButton fab = view.findViewById(R.id.fab);
        final String csv = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/StudentAttendance.csv");
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CSVWriter writer = null;
                try {
                    writer = new CSVWriter(new FileWriter(csv));

                    List<String[]> data = new ArrayList<String[]>();
                    data.add(new String[]{"Fecha","Name", "Code"});

                    if( !studentsAbscence.isEmpty()){
                        for (String key: fechas){
                            List<Student> students = studentsAbscence.get(key);
                            for (Student student: students){
                                Log.w(TAG_PRUEBA, student.getName() + "/" + student.getId());
                                data.add(new String[]{key, student.getName(), student.getId()});
                            }
                        }
                    } else {
                        for (String key: listCategorias){
                            List<Student> students = mapChild.get(key);
                            for (Student student: students){
                                Log.w(TAG_PRUEBA, student.getName() + "/" + student.getId());
                                data.add(new String[]{key, student.getName(), student.getId()});
                            }
                        }
                    }


                    writer.writeAll(data); // data is adding to csv

                    writer.close();

                    Toast.makeText(getContext(),"Your File is save in storage", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void httpRequest(int method, String url, JSONObject jsonValue, final VolleyCallback callback) {

        MySingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        JsonObjectRequest response = new JsonObjectRequest(method, url, jsonValue, new Response.Listener <JSONObject> () {

            @Override
            public void onResponse(JSONObject Response) {
                callback.onSuccessResponse(Response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
               callback.onErrorResponse(e);
            }
        });
        MySingletonRequestQueue.getInstance(getContext()).addToRequestQueue(response);
    }
}
